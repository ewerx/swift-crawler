// swift-tools-version:4.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "crawler",
    dependencies: [
        .package(url: "https://github.com/scinfu/SwiftSoup.git", from: "1.5.5")
    ],
    targets: [
        .target(
            name: "crawler",
            dependencies: ["CrawlerCore"]
        ),
        .target(
            name: "CrawlerCore",
            dependencies: ["SwiftSoup"]
        ),
        .testTarget(
            name: "CrawlerTests",
            dependencies: ["CrawlerCore"]
        )
    ]
)
