# swift-crawler

A simple web crawler written in Swift.

## Installation

### macOS
- Install Xcode 9
- `git clone https://gitlab.com/ewerx/swift-crawler.git`
- `cd swift-crawler`
- `swift build -c release`
- `ln -s .build/release/crawler crawler`

### Ubuntu
- `eval "$(curl -sL https://apt.vapor.sh)"` *# adds repo needed for swift*
- `sudo apt-get install swift`
- `git clone https://gitlab.com/ewerx/swift-crawler.git`
- `cd swift-crawler`
- `swift build -c release`
- `ln -s .build/release/crawler crawler`

## Usage

`./crawler <url>`

*Example*:
`./crawler http://hindsightapp.io`

## Testing
`swift test`
