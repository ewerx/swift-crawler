//
//  Crawler.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import Foundation

/// Command-line processor and launcher
public final class Crawler {
    private let arguments: [String]

    public init(arguments: [String] = CommandLine.arguments) {
        self.arguments = arguments
    }

    public func run() throws {
        guard arguments.count > 1 else {
            throw Error.missingURL
        }

        guard let url = URL(string: arguments[1]),
            url.scheme != nil,
            url.host != nil else {
            throw Error.invalidURL
        }

        let spider = Spider()
        spider.start(url: url)
    }
}

public extension Crawler {
    enum Error: Swift.Error {
        case missingURL
        case invalidURL
    }
}

