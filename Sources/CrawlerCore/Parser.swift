//
//  Parser.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import Foundation
import SwiftSoup


class Parser {
    /// Returns set of links to crawl and set of static asset links
    static func parse(content: String, sourceURL: URL) -> (Set<URL>, Set<URL>) {
        var links: Set<URL> = []
        var assets: Set<URL> = []
        
        do {
            let doc = try SwiftSoup.parse(content, sourceURL.absoluteString)
            
            let hrefs = try doc.select("[href]").map { try $0.attr("abs:href") } // all elements with href attribute
            let srcs = try doc.select("[src]").map { try $0.attr("abs:src") } // all elements with src attribute
            
            let allLinks = hrefs + srcs
            
            // filter out invalid urls
            let validLinks: [URL] = allLinks.flatMap { urlString in
                guard let url = URL(string: urlString),
                    let scheme = url.scheme,
                    scheme == "https" || scheme == "http",
                    url.hasSameHost(asUrl: sourceURL)
                    else { return nil }
                return url
            }
            
            // separate assets and page links
            for link in validLinks {
                switch link.pathExtension {
                case "js", "css", "jpg", "jpeg", "png", "svg", "ico":
                    assets.insert(link)
                default:
                    links.insert(link.withoutFragment)
                }
            }
            
        } catch Exception.Error(let type, let message) {
            print("parse error \(type): \(message)")
        } catch {
            print("unknown parse error")
        }
        
        return (links, assets)
    }
}
