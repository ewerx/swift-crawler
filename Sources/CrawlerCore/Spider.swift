//
//  Spider.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import Foundation
import Dispatch

/// Web Crawler
class Spider {
    var queue: Set<URL> = []
    var visited: Set<URL> = []

    let semaphore = DispatchSemaphore(value: 0)


    func start(url: URL) {
        // hack to format the output as a JSON array while outputting pages as they are crawled
        // if streaming output is not needed this could be refactored to construct JSON of entire output before printng out
        print("[")
        queue = [url]
        crawl()
        semaphore.wait()
        print("]")
    }

    /// Fetch and processes the next page in the queue
    private func crawl() {
        guard let url = queue.popFirst() else {
            // finished crawling
            self.semaphore.signal()
            return
        }
        guard !visited.contains(url) else {
            crawl()
            return
        }

        visited.insert(url)

        fetch(url: url) { content in
            defer {
                self.crawl()
            }

            guard let content = content else {
                return
            }

            let (links, assets) = Parser.parse(content: content, sourceURL: url)

            // add the links to crawl to the queue
            self.queue.formUnion(links)

            // print out assets for the page
            let output = self.generateJSONOutput(url: url, assets: assets)
            print(output + ",")
        }
    }

    private func fetch(url: URL, completion: @escaping (String?) -> Void) {
        let task = URLSession(configuration: URLSessionConfiguration.default).dataTask(with: url) { data, response, error in
            guard error == nil,
                let data = data,
                let content = String(data: data, encoding: .utf8) else {
                    completion(nil)
                    return
            }
            completion(content)
        }
        task.resume()
    }

    /// Output JSON-formatted dictionary of assets for the given URL
    private func generateJSONOutput(url: URL, assets: Set<URL>) -> String {

        struct PageAssets: Codable {
            let url: String
            let assets: [String]
        }

        let pageAssets = PageAssets(url: url.absoluteString,
                                    assets: assets.map { $0.absoluteString })

        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        guard let json = try? encoder.encode(pageAssets),
            let jsonString = String(data: json, encoding: .utf8) else {
                return ""
        }

        // remove extra escaping of / added by JSONEncoder
        return jsonString.replacingOccurrences(of: "\\/", with: "/")
    }
}
