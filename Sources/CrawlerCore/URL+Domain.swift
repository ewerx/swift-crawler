//
//  URL+Domain.swift
//  crawlerPackageDescription
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import Foundation

extension URL {
    /// Returns true if URLs have matching host component
    func hasSameHost(asUrl url: URL) -> Bool {
        return (self.host != nil && self.host == url.host)
    }
    
    /// Returns URL with fragment component removed
    var withoutFragment: URL {
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: false) else { return self }
        components.fragment = nil
        return components.url ?? self
    }
}
