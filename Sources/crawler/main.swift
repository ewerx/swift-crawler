//
//  main.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import CrawlerCore

let crawler = Crawler()

do {
    try crawler.run()
} catch {
    print("Whoops! An error occurred: \(error)")
}
