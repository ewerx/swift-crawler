//
//  CrawlerTests.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-24.
//

import Foundation
import XCTest
import CrawlerCore

// for capturing stdout to test
let stdOutLogFile = "stdout.log"
var savedStdOutHandle: Int32!


class CrawlerTests: XCTestCase {

    func testCommandLineMissingArgument() throws {
        let args: [String] = ["."]
        let crawler = Crawler(arguments: args)

        XCTAssertThrowsError(try crawler.run()) { error in
            XCTAssertEqual(error as? Crawler.Error, Crawler.Error.missingURL)
        }
    }

    func testCommandLineInvalidArgument() throws {
        let args: [String] = [".", "http://"]
        let crawler = Crawler(arguments: args)

        XCTAssertThrowsError(try crawler.run()) { error in
            XCTAssertEqual(error as? Crawler.Error, Crawler.Error.invalidURL)
        }
    }

    func testValidURL() throws {
        let args: [String] = [".", "http://localhost"]
        let crawler = Crawler(arguments: args)

        saveStdOut()
        XCTAssertNoThrow(try crawler.run())
        let stdout = getStdOut()
        XCTAssertEqual(stdout, "[\n]\n", "Unexpected output: \(stdout)")
    }
}

extension XCTestCase {
    // redirect stdout to log file
    func saveStdOut() {
        savedStdOutHandle = dup(STDOUT_FILENO)
        freopen(stdOutLogFile, "w", stdout)
    }

    // read from stdout log file
    func getStdOut() -> String {
        self.restoreStdOut()
        let content = try! String(contentsOfFile: stdOutLogFile, encoding: String.Encoding.utf8)
        return content
    }

    private func restoreStdOut(){
        fflush(stdout)
        dup2(savedStdOutHandle, STDOUT_FILENO)
        close(savedStdOutHandle)
    }
}
