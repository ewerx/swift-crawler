//
//  ParserTests.swift
//  crawler
//
//  Created by Ehsan Rezaie on 2017-10-25.
//

import Foundation
import XCTest
@testable import CrawlerCore

class ParserTests: XCTestCase {
    func testEmptyPage() {
        let content = ""
        testParser(withContent: content, expectedLinks: [], expectedAssets: [])
    }
    
    func testNoLinks() {
        let content = """
                        <html>
                        <head>
                        <body>
                        This is a body with a link in the text: http://www.google.com
                        </body>
                        </head>
                        </html>
                        """
        testParser(withContent: content, expectedLinks: [], expectedAssets: [])
    }
    
    func testAssets() {
        let content = """
                        <html>
                        <head>
                        <link rel="fluid-icon" href="https://google.com/fluidicon.png" title="GitHub">
                        <link rel="mask-icon" href="https://google.com/pinned-octocat.svg" color="#000000">
                        <link rel="icon" type="image/x-icon" class="js-site-favicon" href="https://google.com/favicon.ico">
                        <body>
                        <script crossorigin="anonymous" integrity="sha256-kO4IRl30AFXbobbN8akpieAMTwfYBby7HBg788ye2vQ=" src="https://google.com/assets/frameworks.js"></script>
                        </body>
                        </head>
                        </html>
                        """
        testParser(withContent: content,
                   expectedLinks: [],
                   expectedAssets: [
                    "https://google.com/fluidicon.png",
                    "https://google.com/pinned-octocat.svg",
                    "https://google.com/favicon.ico",
                    "https://google.com/assets/frameworks.js"
            ])
    }
    
    func testLinks() {
        let content = """
                        <html>
                        <head>
                        <body>
                        <a href="http://google.com/page1">Page 1</a>
                        <a href="http://google.com/page1">Page 1</a>
                        <a href="http://google.com/page1/2">Page 1.2</a>
                        <a href="http://google.com/page2/1">Page 2.1</a>
                        <a href="http://google.com/page2/1">Page 2.1</a>
                        <a href="http://google.com/page3/1/1">Page 3.1.1</a>
                        <a href="http://google.com/page3/1/1">Page 3.1.1</a>
                        <a href="http://google.com/page3/1/2">Page 3.1.2</a>
                        <a href="http://google.com/page3/1/2">Page 3.1.2</a>
                        </body>
                        </head>
                        </html>
                        """
        testParser(withContent: content,
                   expectedLinks: [
                    "http://google.com/page1",
                    "http://google.com/page1/2",
                    "http://google.com/page2/1",
                    "http://google.com/page3/1/1",
                    "http://google.com/page3/1/2"
                    ],
                   expectedAssets: [])
    }
    
    func testFragmentLinks() {
        let content = """
                        <html>
                        <head>
                        </head>
                        <body>
                        <a href="http://google.com/page1#frag1">Page 1.1</a>
                        <a href="http://google.com/page1#frag2">Page 1.2</a>
                        <a href="http://google.com/page2#frag1">Page 2.1</a>
                        <a href="http://google.com/page3#frag3">Page 3.3</a>
                        <a href="http://google.com/page4#frag4">Page 4.4</a>
                        <a href="http://google.com/page4#frag5">Page 4.5</a>
                        </body>
                        </html>
                        """
        testParser(withContent: content,
                   expectedLinks: [
                    "http://google.com/page1",
                    "http://google.com/page2",
                    "http://google.com/page3",
                    "http://google.com/page4",
                    ],
                   expectedAssets: [])
    }
    
    func testQueryLinks() {
        let content = """
                        <html>
                        <head>
                        </head>
                        <body>
                        <a href="http://google.com?query=1&param=1">...</a>
                        <a href="http://google.com/index.html?query=1&param=2">....</a>
                        <a href="http://google.com/a/b/c/d/e/f?query=2&param=1">...</a>
                        <a href="http://google.com/a/b/c/d/e/f?query=3&param=1">...</a>
                        </body>
                        </html>
                        """
        testParser(withContent: content,
                   expectedLinks: [
                    "http://google.com?query=1&param=1",
                    "http://google.com/index.html?query=1&param=2",
                    "http://google.com/a/b/c/d/e/f?query=2&param=1",
                    "http://google.com/a/b/c/d/e/f?query=3&param=1"
                    ],
                   expectedAssets: [])
    }
    
    func testDomains() {
        let content = """
                        <html>
                        <head>
                        <link rel="fluid-icon" href="https://google.com/fluidicon.png" title="GitHub">
                        <link rel="mask-icon" href="https://google.com/pinned-octocat.svg" color="#000000">
                        <link rel="icon" type="image/x-icon" class="js-site-favicon" href="https://google.com/favicon.ico">
                        <link rel="fluid-icon" href="https://maps.google.com/fluidicon2.png" title="GitHub">
                        <link rel="mask-icon" href="https://www.google.com/pinned-octocat2.svg" color="#000000">
                        <link rel="icon" type="image/x-icon" class="js-site-favicon" href="https://yahoo.com/favicon2.ico">
                        </head>
                        <body>
                        <script crossorigin="anonymous" src="https://www.google.com/assets/www.js"></script>
                        <script crossorigin="anonymous" src="https://google.com/assets/google.js"></script>
                        <script crossorigin="anonymous" src="https://yahoo.com/assets/yahoo.js"></script>
                        <a href="http://google.com/page1">...</a>
                        <a href="http://google.com/page2">...</a>
                        <a href="http://maps.google.com/page3">...</a>
                        <a href="http://mail.google.com/page4">...</a>
                        <a href="http://www.google.com/page5?query=1&param=1">...</a>
                        <a href="http://google.ca/index.html">...</a>
                        <a href="http://yahoo.com/index.html">...</a>
                        </body>
                        </html>
                        """
        testParser(withContent: content,
                   expectedLinks: [
                    "http://google.com/page1",
                    "http://google.com/page2"
                    ],
                   expectedAssets: [
                    "https://google.com/fluidicon.png",
                    "https://google.com/pinned-octocat.svg",
                    "https://google.com/favicon.ico",
                    "https://google.com/assets/google.js",
                    
            ])
    }
    
    private func testParser(withContent content: String, expectedLinks: [String], expectedAssets: [String]) {
        let (links, assets) = Parser.parse(content: content, sourceURL: URL(string: "https://google.com")!)
        XCTAssert(links == Set(expectedLinks.flatMap { URL(string: $0) }), "\(links)")
        XCTAssert(assets == Set(expectedAssets.flatMap { URL(string: $0) }), "\(assets)")
    }
}
