// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import XCTest
@testable import CrawlerTests

extension CrawlerTests {
  static var allTests = [
    ("testCommandLineMissingArgument", testCommandLineMissingArgument),
    ("testCommandLineInvalidArgument", testCommandLineInvalidArgument),
    ("testValidURL", testValidURL),
  ]
}

extension ParserTests {
  static var allTests = [
    ("testEmptyPage", testEmptyPage),
    ("testNoLinks", testNoLinks),
    ("testAssets", testAssets),
    ("testLinks", testLinks),
    ("testFragmentLinks", testFragmentLinks),
    ("testQueryLinks", testQueryLinks),
    ("testDomains", testDomains),
  ]
}


XCTMain([
  testCase(CrawlerTests.allTests),
  testCase(ParserTests.allTests),
])
